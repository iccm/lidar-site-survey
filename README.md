# LIDAR Site Survey

A pipeline for generating surveys of community mesh network sites.

## Dependencies

- a BASH-compatible shell
- [curl](https://curl.se)
- [7-Zip](https://www.7-zip.org)
- [GDAL](https://gdal.org)
- [PDAL](https://pdal.io)
- [Signal-Server](https://github.com/Cloud-RF/Signal-Server)*
- [Task](https://taskfile.dev)*

Marked (*) dependencies will probably need to be built from source; everything else is likely to be available via your OS package repositories.

## Usage

### Preparing sites for profiling

```bash
# Build all .site files in the sites/ directory
task 

# Build a single site defined in sites/<SITE_ID>.site
task build-site SITE_ID=<SITE_ID>

# Remove all generated outputs
task clean

# Remove all downloaded and generated outputs
task fullclean

# View all available tasks
task -a
```

### Profiling sites

```bash
cd sites/<SITE_ID>

# <lat> and <lon> are GPS coords, <txh> is transmitter height in meters
# Outputs <SITE_ID>_2GHz.png and <SITE_ID>_5GHz.png
./profile_site.sh <lat> <lon> <txh>
